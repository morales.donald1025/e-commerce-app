//import state hook from react
//import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Row, Col, Card } from "react-bootstrap";
import Button  from "react-bootstrap/Button";
import { Link } from "react-router-dom";

export default function ProductCard({productProp}){


const {_id, name, description, price} = productProp;


 return (

  
    <Col xs={12} md={4} className="mt-5">
          <Card className="cardHighlight p-3">
  <Card.Body>
    <Card.Title>
      <h3 className="text-center">{name}</h3>
    </Card.Title>
    <Card.Subtitle>Description:</Card.Subtitle>
    <Card.Text>{description}</Card.Text>
    <Card.Subtitle>Price:</Card.Subtitle>
    <Card.Text>PHP {price}</Card.Text>
    


    
  </Card.Body>
</Card>
</Col>


    
    )    
}
