import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useHistory, Link } from "react-router-dom";
import userContext from "../UserContext";
import Swal from "sweetalert2";
import ProductsLoginNav from "./ProductsLoginNav";
//import Basket from "../TestComponents/Basket"






export default function ViewProductLogin(props){
	



const { user } = useContext(userContext);
const history = useHistory();
	//The useParams hook allows us to retrieve the courseId passed via URL

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imageUrl, setImageUrl] = useState("")
	const [quantity, setQuantity] = useState(1);
	const [email, setEmail] = useState("luffy@mail.com");




			








 

/*
const onAdd = (productId) => {
    const exist = cartItems.find((x) => x.id === productId);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === productId ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...productId, qty: 1 }]);
    }
  };
  const onRemove = (productId) => {
    const exist = cartItems.find((x) => x.id === productId);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== productId));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === productId ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };*/
//https://vast-island-07766.herokuapp.com/api/order/users/checkout

//http://localhost:4000/api/order/cart
//http://localhost:4000/api/order/users/checkout


const addToCart = (productId) => {
	fetch("https://vast-island-07766.herokuapp.com/api/order/users/checkout", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			productId: productId,
			price: price,
			quantity: quantity,
			email: email
		})
	})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			/*console.log(email);
			console.log(user.id);*/



			if(data !== true){
				Swal.fire({
					title: "Successfully purchased an order",
					icon: "success",
					text: "You have successfully ordered this product"
				})

				history.push("/productslogin");
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
}

//The useEffect hook is used to check if the courseId is retrieved properly

useEffect(() => {
	console.log(productId);

	fetch(`https://vast-island-07766.herokuapp.com/api/product/products/${productId}`)
	.then(res => res.json())
	.then(data => {
		console.log(data);
		setEmail(user.email);
		setName(data.name);
		setDescription(data.description);
		setPrice(data.price);
		setImageUrl(data.imageUrl);
		//setQuantity(data.quantity)

	})

}, [productId]);

	return(
		


		<Row className="mt-3 mb-3 container-fit justify-content-center">
		<Col xs={12} md={4} className="mt-5">
    
          <Card className="cardHighlight p-3">

          <img className="small" src={imageUrl} alt="Logo" />
          
          
  <Card.Body>

    <Card.Title>
      <h3 className="text-center" >{name}</h3>
    </Card.Title>
    <Card.Subtitle className="text-center">Description:</Card.Subtitle>
    <Card.Text className="text-center">{description}</Card.Text>
    <Card.Subtitle className="text-center">Price:</Card.Subtitle>
    <Card.Text className="text-center">PHP {price}</Card.Text>
    
  
      
  </Card.Body>
  <button onClick={() => addToCart(productId)}>Buy Now!</button>
</Card>
</Col>



</Row>



		)
}