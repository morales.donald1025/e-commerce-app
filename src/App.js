import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AllActiveFinal from "./admin/adminPages/AllActiveFinal"
import AddProductFinal from "./admin/AddProductFinal"
import EditProductFinal from "./admin/EditProductFinal"
import AllOrders from "./admin/adminPages/Allorders"
import AdminLogout from "./admin/AdminLogout"


import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"
import './App.css';
import { Container } from 'react-bootstrap';
import Register from "./pages/Register"
import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import Products from "./pages/Products"
import Login from "./pages/Login"
import ProductsLogin from "./pages/ProductsLogin"
import Logout from './pages/Logout';
import Checkout from "./pages/Checkout"
import IndexCheckout from "./pages/IndexCheckout"
import ViewProductLogin from "./components/ViewProductLogin"
import ViewProductDetail from "./pages/ViewProductDetail"
import ViewCartDetails from "./pages/ViewCartDetails"
import AppTest from "./AppTest"
import CartView from "./pages/CartView"
import ThankYou from "./pages/ThankYou"
import MyOrders from "./components/MyOrders"
import MyOrdersFetch from "./pages/MyOrdersFetch";
import AdminLandingPage from "./admin/adminPages/AdminLandingPage"
import AdminPageAfterLogin from "./admin/adminPages/AdminPageAfterLogin"
import AdminArchievedFinal from "./admin/adminPages/AdminArchievedFinal"



function App() {
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null,
    email: null
  })


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])



  return (
<UserProvider value={{user, setUser, unsetUser}}>

<Router>
         
         <Container>
           <Switch>
             <Route exact path="/" component={Home} />
             <Route exact path="/login" component={Login} />
             <Route exact path="/register" component={Register} />
             <Route exact path="/products" component={Products} />
             <Route exact path="/logout" component={Logout} />
             <Route exact path="/productslogin" component={ProductsLogin} />
             <Route exact path="/api/product/products/:productId" component={ViewProductDetail} />
             <Route exact path="/api/order/cart" component={ViewCartDetails} />
             <Route exact path="/api/products/login/cart" component={AppTest} />
             <Route exact path="/checkOut" component={Checkout} />
             <Route exact path="/test/checkOut" component={IndexCheckout} />
             <Route exact path="/thankyou" component={ThankYou} />
             <Route exact path="/MyOrders" component={MyOrders} />
             <Route exact path="/MyOrdersTest" component={MyOrdersFetch} />
             <Route exact path="/api/products/login/MyOrderTest" component={MyOrdersFetch} />
             <Route exact path="/admin" component={AdminLandingPage} />
             <Route exact path="/AdminPageAfterLogin" component={AdminPageAfterLogin} />
             <Route exact path="/archieved/:productId" component={AdminArchievedFinal} />
             <Route exact path="/adminproducts" component={AllActiveFinal} />
             <Route exact path="/addProduct" component={AddProductFinal} />
             <Route exact path="/allOrders" component={AllOrders} />
             <Route exact path="/AdminLogout" component={AdminLogout} />
             <Route exact path="/admin/edit/:productId" component={EditProductFinal} />
           </Switch>
            </Container>
       </Router>
         

    </UserProvider>
  );
}

export default App;





