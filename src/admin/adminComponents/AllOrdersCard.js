import { Container, Table } from 'react-bootstrap';


import Button  from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { Row, Col, Card } from "react-bootstrap";

export default function AllOrdersCard({orderProp}){

const {_id, product, name, description, price, purchasedOn, quantity} = orderProp;


 return (
//<Col xs={12} md={4} className="mt-5">

         <Container>
<Card className="cardHighlight p-3">
      <div className="text-center text-white my-4">
        <h2>My Orders</h2>
              
      </div>

      <Table striped bordered hover responsive>
        <thead className="bg-dark text-white">
          <tr>
            <th>ProductId</th>
            <th>Quantity</th>
            <th>PurchasedOn</th>
            <th>Price</th>
            
          </tr>         
        </thead>
        <tbody className="bg-dark">
          <tr className="text-black">
          
          <td className="text-white">{product}</td>
          
          <td className="text-white">{quantity}</td>
          <td className="text-white">{purchasedOn}</td>
          <td className="text-white">{price}</td>
        </tr>
        </tbody>
      </Table>

               
      
      </Card>
    </Container>

    

		)    
}










  
